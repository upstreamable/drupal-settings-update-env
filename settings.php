<?php

$config['update.settings']['notification']['emails'] = explode(',', getenv('DRUPAL_UPDATE_NOTIFICATION_EMAILS'));
$config['update.settings']['notification']['threshold'] = getenv('DRUPAL_UPDATE_THRESHOLD') ?: 'all';
$config['update.settings']['check']['disabled_extensions'] = getenv('DRUPAL_UPDATE_CHECK_DISABLED_EXTENSIONS') ?: false;
$config['update.settings']['check']['interval_days'] = getenv('DRUPAL_UPDATE_CHECK_INTERVAL_DAYS') ?: 1;
